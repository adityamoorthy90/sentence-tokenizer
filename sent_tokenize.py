from nltk.tokenize.punkt import PunktSentenceTokenizer
import os
import sys
import pickle
import logging
import config as cfg

program = os.path.basename(sys.argv[0])
logger = logging.getLogger(program)
logging.basicConfig(format='%(asctime)s | %(message)s')
logging.root.setLevel(level=logging.INFO)

input_corpus = 'B.1—Securities 1 (“Main Schedule”) The Securities 1 schedule collects individual security-level ' \
               'details on positions, security type, cumulative OTTI (credit and non-credit related impairments) by ' \
               'security, and accounting intent (AFS or HTM). Amounts should be reported in U.S. dollars (USD). The ' \
               'reporting of Securities should follow balance sheet classification of the FR Y-9C (e.g., Securities ' \
               'will correspond with Schedule HC-B breakdowns). Additionally, the method of reporting individual ' \
               'security-level information should be consistent with the level of aggregation the company uses to ' \
               'assess impairment and measure realized and unrealized gains and losses on investment securities ' \
               'under GAAP (ASC paragraph 320-10-35-20).'

with open(cfg.CORPUS_FILENAME, 'r', encoding='utf-8', errors='ignore') as f:
    train_corpus = f.read()

# Check if there is an existing tokenizer already trained
if os.path.exists(cfg.TOKENIZER_FILENAME):
    logger.info('Loading...')
    with open(cfg.TOKENIZER_FILENAME, 'rb') as punkt_file:
        tokenizer = pickle.load(punkt_file)
else:
    logger.info('Training...')
    tokenizer = PunktSentenceTokenizer(train_corpus)
    with open(cfg.TOKENIZER_FILENAME, 'wb') as punkt_file:
        pickle.dump(tokenizer, punkt_file, pickle.HIGHEST_PROTOCOL)
logger.info('Transforming...')
output_corpus = tokenizer.tokenize(input_corpus)
logger.info('\n'.join(output_corpus))
